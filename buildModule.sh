#!/bin/bash
#
# Build the module for UNIX
#

if [ "$1" = "" ] ; then echo "Usage: $0 <keyAlias> [flavour [version [keystore]]]"; exit 1; fi

#
# Functions
#
function init() {
	finit "$tmp"
	mkdir -p "$tmp"
}

function finit() {
        if [ -d "$tmp" ] ; then
                rm -rf  "$tmp"
        fi
	rm -f "temp${file}.zip"

}
function title() {
	echo
	echo "$@"
	echo
}

#
# Variables declaration
#
tmp="/tmp/screencam-module${RANDOM}${RANDOM}"
keyAlias="$1"
flavour="$2"
version="$3"
if [ "$flavour" = "" ] ; then flavour="1"; fi
if [ "$version" = "" ] ; then version="1"; fi
file="ScreenCam-Magisk-V$version-$flavour"
keyStore="$HOME/.keystore"
if ! [ "$4" = "" ] ; then keyStore="$4"; fi

#
# Read password
#
if ! [ "$PASSWD" = "" ] ; then
	keyPass="$PASSWD"
	aliasPass="$PASSWD"
else
	read -s -p "Enter Keystore Password: " keyPass
	read -s -p "Enter Alias Password. Leave empty to use keystore password: " aliasPass
fi

#
# Module building
#
init

title Zipping module....
7z a -tzip "temp${file}.zip" * -xr\!.* -xr\!Releases -xr\!temp -xr\!*.bat -xr\!buildModule.* -xr\!*.zip -xr\!PLACEHOLDER 1> /dev/null || exit 1

title Signing zip
jarsigner -keystore "${keyStore}" -storepass "${keyPass}" -sigfile CERT -tsa http://timestamp.comodoca.com/rfc3161 -digestalg SHA-1 -signedjar "Releases${file}-Signed.zip" "temp${file}.zip" "${keyAlias}" --key-pass "${aliasPass}"
rm -f "temp${file}.zip"

finit
